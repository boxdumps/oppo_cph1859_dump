## cesium_CPH1859-userdebug 11 RQ3A.210905.001 eng.kardeb.20210926.125406 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: CPH1859
- Brand: oppo
- Flavor: cesium_CPH1859-userdebug
- Release Version: 11
- Id: RQ3A.210905.001
- Incremental: eng.kardeb.20210926.125406
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oppo/cesium_CPH1859/CPH1859:11/RQ3A.210905.001/kardebayan309261253:userdebug/release-keys
- OTA version: 
- Branch: cesium_CPH1859-userdebug-11-RQ3A.210905.001-eng.kardeb.20210926.125406-release-keys
- Repo: oppo_cph1859_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
